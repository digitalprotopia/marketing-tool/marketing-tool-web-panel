import App from 'next/app';
import {
  ApolloProvider, gql, useMutation, useQuery,
} from '@apollo/client';
import Head from 'next/head';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Link from 'next/link';
import {
  Box, IconButton,
} from '@mui/material';
import { useRouter } from 'next/router';
import Button from '@mui/material/Button';
import { useRef, useState } from 'react';
import Popover from '@mui/material/Popover';
import { SnackbarProvider, useSnackbar } from 'notistack';
import { KeyboardArrowDown } from '@mui/icons-material';
import Image from 'next/image';
import client from '../components/apollo-client';
import Logo from '../assets/digitalprotopia.jpg';

function DropdownIcon(props) {
  return (
    <Box
      {...props}
      component="span"
      sx={{
        px: 0.5,
        stroke: '#859CBE',
        display: 'flex',
        alignItems: 'center',
      }}
    />
  );
}

const theme = createTheme({
  palette: {
    primary: {
      main: '#F79244',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#DAE5F4',
      contrastText: '#4B5A73',
    },
    tertiary: {
      main: '#687B98',
      contrastText: '#4B5A73',
    },
  },
  variables: {
    leftPanel: {
      width: 250,
    },
    header: {
      height: 73,
    },
  },
  components: {
    MuiSelect: {
      defaultProps: {
        IconComponent: DropdownIcon,
      },
    },
  },
});

const GET_ME = gql`
query {
  me {
    id
    name
    role
  }
}
  `;

const LOG_OUT = gql`
mutation {
  logOut
}
`;

function S3App({ Component, pageProps }) {
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();
  try {
    window.enqueueSnackbar = enqueueSnackbar;
    window.router = router;
  } catch (e) {
    //
  }

  const [logOut] = useMutation(LOG_OUT);

  const userPanelRef = useRef(null);
  const [userPanelOpen, setUserPanelOpen] = useState(false);

  const getMe = useQuery(GET_ME);
  let {
    data,
  } = getMe;
  const {
    refetch,
    error,
    loading,
  } = getMe;
  if (!loading && (error || !data?.me)) {
    localStorage.removeItem('token');
    data = null;
  }

  const allowedPathsGuest = ['/', '/login', '/register', '/restore-password/request', '/restore-password/confirm', '/confirm-email'];
  const disallowedPathsUser = ['/login', '/register', '/restore-password/request', '/restore-password/confirm'];

  if (loading) {
    return null;
  }

  if (
    !loading
    && !data?.me
    && !allowedPathsGuest.some((path) => router.pathname.includes(path))

  ) {
    router.push('/login');
    return null;
  }

  if (
    !loading
    && data
    && disallowedPathsUser.some((path) => router.pathname.includes(path))

  ) {
    router.push('/');
    return null;
  }

  return (
    <>
      <style>
        {`
            html, body {
              height: 100%
            }
            .issuesTable .MuiTableCell-root {
              border: 1px solid #DAE5F4;
            }
            #__next, #app, main, main > div {
              height: 100%
            }
            #__next > div {
              display: flex;
              flex-direction: column;
            }
            .container {
              flex-grow: 1;
            }
            body {
              font-family: 'Roboto', sans-serif;
              font-size: 12px;
              margin: 0;
            }
            a {
              color: inherit;
              text-decoration: none;
            }
            h2 { font-size: 20px; }
            h3 { font-size: 16px; }
            h4 { font-size: 14px; }
            .domainTable > tbody > tr > td {
              padding-bottom: 20px;
            }
      `}
      </style>
      <div id="app">
        <Head>
          <title>S3App</title>
          <link rel="icon" href="/favicon.ico" />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <meta name="viewport" content="width=1200" />
        </Head>
        <Box
          sx={{
            fontSize: '16px',
            display: 'flex',
            alignItems: 'center',
            borderBottom: data?.me ? '#687B98 1px solid' : 'none',
            height: theme.variables.header.height - 1,
            justifyContent: 'space-between',
            padding: '0px 16px',
          }}
        >
          <IconButton disableRipple href="/">
            <Image src={Logo} alt="" style={{ width: 42, height: 'auto' }} />
          </IconButton>

          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            {data?.me ? (
              <>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: 8,
                  }}
                >
                  <Link href="/cart">
                    <Button variant="contained">
                      Корзина
                    </Button>
                  </Link>
                  <Link href="/orders">
                    <Button variant="contained">
                      Заказы
                    </Button>
                  </Link>
                  <Link href="/licenses">
                    <Button variant="contained">
                      Ключи
                    </Button>
                  </Link>
                </div>
                <Link href="/account">
                  {/* <S3Avatar name={data?.me.name} sx={{ cursor: 'pointer' }} /> */}
                </Link>
                <Link href="/account">
                  <span
                    style={{ color: theme.palette.primary.main, marginLeft: 8, cursor: 'pointer' }}
                  >
                    {`${data?.me.name} (${data?.me.role})`}
                  </span>
                </Link>
                <IconButton
                  ref={userPanelRef}
                  sx={{ stroke: theme.palette.primary.main, marginRight: 2 }}
                  onClick={() => setUserPanelOpen(!userPanelOpen)}
                >
                  <KeyboardArrowDown />
                </IconButton>
                <Popover
                  open={userPanelOpen}
                  anchorEl={userPanelRef.current}
                  onClose={() => setUserPanelOpen(false)}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={async () => {
                      await logOut();
                      setUserPanelOpen(false);
                      localStorage.removeItem('token');
                      try {
                        refetch();
                      } catch (e) {
                        console.log(e);
                      }
                      router.push('/login');
                    }}
                  >
                    Выйти
                  </Button>
                </Popover>
              </>
            ) : <Link href="/login">Войти</Link>}
          </Box>
        </Box>
        <div className="container">
          <main>
            <Component
              {...pageProps}
              user={data?.me}
              refetchUser={refetch}
            />
          </main>
        </div>
      </div>
    </>
  );
}

export default function S3AppContainer(props) {
  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={3}>
          <S3App {...props} />
        </SnackbarProvider>
      </ThemeProvider>
    </ApolloProvider>
  );
}

S3AppContainer.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);
  return { ...appProps };
};
