import {
  Button, Container, TextField, Typography, Box,
} from '@mui/material';
import Link from '@mui/material/Link';
import { gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useSnackbar } from 'notistack';

const SEND_PASSWORD_RECOVER_LINK = gql`
    mutation($email: String!) {
        sendRecoverPasswordLink(email: $email)
    }
`;

export default function Request() {
  const { enqueueSnackbar } = useSnackbar();
  const [sendPasswordRecoverLink] = useMutation(SEND_PASSWORD_RECOVER_LINK);
  const [email, setEmail] = useState('');

  return (
    <Container
      maxWidth="xs"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={(theme) => ({
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mb: `${theme.variables.header.height}px`,
        })}
      >
        <Typography component="h1" variant="h5">
          Восстановление пароля
        </Typography>
        <Box sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            fullWidth
            label="E-Mail"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Button
            fullWidth
            variant="contained"
            sx={{ mt: 1 }}
            disabled={!email}
            onClick={async () => {
              try {
                await sendPasswordRecoverLink({ variables: { email } });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
                return;
              }
              enqueueSnackbar('Ссылка для восстановления пароля отправлена на указанный E-Mail', { variant: 'success' });
            }}
          >
            Выслать ссылку
          </Button>
          <Box sx={{
            display: 'flex', justifyContent: 'space-between', pt: '10px', fontSize: 15,
          }}
          >
            <Link href="/login" sx={{ textDecoration: 'none' }}>Войти</Link>
            <Link href="/register" sx={{ textDecoration: 'none' }}>Зарегистрироваться</Link>
          </Box>
        </Box>
      </Box>
    </Container>
  );
}
