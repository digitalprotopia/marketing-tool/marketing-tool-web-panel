import { gql, useMutation } from '@apollo/client';
import Head from 'next/head';
import { useState } from 'react';
import { Button, TextField, Typography } from '@mui/material';
import { useSnackbar } from 'notistack';
import moment from 'moment';
import config from '../../config/config';

const EDIT_ME = gql`
    mutation($user: UserInput!) {
        editMe(user: $user) {
            id
            name
        }
    }
  `;

const CHANGE_PASSWORD = gql`
  mutation($oldPassword: String!, $newPassword: String!) {
    changePassword(oldPassword: $oldPassword, newPassword: $newPassword) {
      id
      name
    }
  }
`;

const SEND_EMAIL_CONFIRMATION_LINK = gql`
  mutation($email: String!) {
    sendEmailConfirmationLink(email: $email)
  }
`;

export default function Account(props) {
  const { enqueueSnackbar } = useSnackbar();

  const [form, setForm] = useState({
    name: props.user.name,
    email: '',
    password: '',
    passwordConfirm: '',
  });
  const [passwordForm, setPasswordForm] = useState({
    oldPassword: '',
    newPassword: '',
    newPasswordConfirm: '',
  });

  const [editMe] = useMutation(EDIT_ME);
  const [changePassword] = useMutation(CHANGE_PASSWORD);
  const [sendEmailConfirmationLink] = useMutation(SEND_EMAIL_CONFIRMATION_LINK);

  return (
    <div style={{ margin: 36 }}>
      <Head>
        <title>Аккаунт</title>
      </Head>
      <div style={{ display: 'flex', flexDirection: 'column', gap: 8 }}>
        {config.license && (
        <div>
          Ключ до
          {' '}
          {moment(props.user.licenseExpires).format('hh:mm:ss DD.MM.YYYY')}
        </div>
        )}
        <Typography lineHeight={1} variant="h6">
          Редактировать данные
        </Typography>
        <div>
          <TextField
            label="Имя"
            value={form.name}
            onChange={(e) => setForm({ ...form, name: e.target.value })}
            variant="standard"
          />
        </div>
        <div style={{ marginBottom: '40px' }}>
          <Button
            variant="contained"
            disabled={
                form.name === props.user.name
            }
            onClick={async () => {
              await editMe({
                variables: { user: { name: form.name } },
              });
              await props.refetchUser();
              enqueueSnackbar('Данные успешно изменены', { variant: 'success' });
            }}
          >
            Сохранить
          </Button>
        </div>
        <Typography lineHeight={1} variant="h6">
          Сменить email
        </Typography>
        <div>
          <TextField
            label="Новый email"
            value={form.email}
            onChange={(e) => setForm({ ...form, email: e.target.value })}
            variant="standard"
          />
        </div>
        <div style={{ marginBottom: '40px' }}>
          <Button
            variant="contained"
            disabled={
                form.email === props.user.email
            }
            onClick={async () => {
              await sendEmailConfirmationLink({ variables: { email: form.email } });
              enqueueSnackbar('Письмо с подтверждением отправлено', { variant: 'success' });
            }}
          >
            Сохранить
          </Button>
        </div>
        <Typography lineHeight={1} variant="h6">
          Сменить пароль
        </Typography>
        <div>
          <TextField
            label="Старый пароль"
            value={passwordForm.oldPassword}
            onChange={(e) => setPasswordForm({ ...passwordForm, oldPassword: e.target.value })}
            variant="standard"
            type="password"
          />
        </div>
        <div>
          <TextField
            label="Новый пароль"
            value={passwordForm.newPassword}
            onChange={(e) => setPasswordForm({ ...passwordForm, newPassword: e.target.value })}
            variant="standard"
            type="password"
          />
        </div>
        <div>
          <TextField
            label="Подтвердите новый пароль"
            value={passwordForm.newPasswordConfirm}
            onChange={(e) => setPasswordForm(
              { ...passwordForm, newPasswordConfirm: e.target.value },
            )}
            variant="standard"
            type="password"
          />
        </div>
        <div style={{ marginBottom: '40px' }}>
          <Button
            variant="contained"
            disabled={
                passwordForm.newPassword !== passwordForm.newPasswordConfirm
                || passwordForm.newPassword === ''
                || passwordForm.oldPassword === ''
            }
            onClick={async () => {
              try {
                await changePassword({
                  variables: {
                    oldPassword: passwordForm.oldPassword,
                    newPassword: passwordForm.newPassword,
                  },
                });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
                return;
              }
              await props.refetchUser();
              enqueueSnackbar('Пароль успешно изменен', { variant: 'success' });
            }}
          >
            Сохранить
          </Button>
        </div>
      </div>
    </div>
  );
}
