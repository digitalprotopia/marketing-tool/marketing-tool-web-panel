import { useSnackbar } from 'notistack';
import { gql, useMutation } from '@apollo/client';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { useTheme } from '@mui/material/styles';
import { useState } from 'react';
import { useRouter } from 'next/router';
import config from '../config/config';

const SIGN_IN = gql`
mutation($password: String!, $email: String, $name: String) {
  signIn(email: $email, name: $name, password: $password)
}
`;

export default function Login(props) {
  const router = useRouter();
  const theme = useTheme();

  const [loginForm, setLoginForm] = useState({
    email: '',
    password: '',
  });
  const [signIn] = useMutation(SIGN_IN);
  const { enqueueSnackbar } = useSnackbar();

  if (props?.user?.domains.length > 0) {
    router.push(`/domains/${props.user.domains[0].id}`);
  }

  if (props?.user?.domains.length === 0) {
    if (config.demo) router.push('/');
    else router.push('/welcomePage');
  }

  return (
    <Container
      maxWidth="xs"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mb: `${theme.variables.header.height}px`,
        }}
      >
        <Typography component="h1" variant="h5">
          Вход
        </Typography>
        <Box sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            fullWidth
            label="E-Mail"
            type="email"
            value={loginForm.email}
            onChange={(e) => setLoginForm({ ...loginForm, email: e.target.value })}
          />
          <TextField
            margin="normal"
            fullWidth
            label="Пароль"
            type="password"
            value={loginForm.password}
            onChange={(e) => setLoginForm({ ...loginForm, password: e.target.value })}
          />
          <Button
            fullWidth
            variant="contained"
            sx={{ mt: 1 }}
            disabled={!loginForm.email || !loginForm.password}
            onClick={async () => {
              try {
                await signIn({
                  variables: { ...loginForm },
                  onCompleted: async (data) => {
                    enqueueSnackbar('Вы вошли', { variant: 'success' });
                    localStorage.setItem('token', data.signIn);
                    props.refetchUser();
                  },
                });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
              }
            }}
          >
            Войти
          </Button>
          <Box sx={{
            display: 'flex', justifyContent: 'space-between', pt: '10px', fontSize: 15,
          }}
          >
            <Link href="restore-password/request" sx={{ textDecoration: 'none', color: 'gray' }}>Забыли пароль?</Link>
            <Link href="register" sx={{ textDecoration: 'none' }}>Зарегистрироваться</Link>
          </Box>
        </Box>
      </Box>
    </Container>
  );
}
