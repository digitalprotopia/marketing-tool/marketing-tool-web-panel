import { useSnackbar } from 'notistack';
import { gql, useMutation } from '@apollo/client';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { useTheme } from '@mui/material/styles';
import { useState } from 'react';
import { useRouter } from 'next/router';
import config from '../config/config';

const SIGN_UP = gql`
mutation($name: String!, $email: String!, $password: String!${config.license ? ', $license: String' : ''}) {
  signUp(name: $name, email: $email, password: $password${config.license ? ', license: $license' : ''}) {
    id
  }
}
`;

export default function Register(props) {
  const router = useRouter();
  const theme = useTheme();

  const [registerForm, setRegisterForm] = useState({
    name: '',
    email: '',
    password: '',
    license: '',
  });
  const [signUp] = useMutation(SIGN_UP);
  const { enqueueSnackbar } = useSnackbar();

  if (props?.user?.domains.length > 0) {
    router.push(`/domains/${props.user.domains[0].id}`);
  }

  if (props?.user?.domains.length === 0) {
    if (config.demo) router.push('/');
    else router.push('/welcomePage');
  }

  return (
    <Container
      maxWidth="xs"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mb: `${theme.variables.header.height}px`,
        }}
      >
        <Typography variant="h5">
          Регистрация
        </Typography>
        <Box sx={{ mt: 1, textAlign: 'center' }}>
          <TextField
            margin="normal"
            fullWidth
            label="Имя"
            name="name"
            value={registerForm.name}
            onChange={(e) => setRegisterForm({ ...registerForm, name: e.target.value })}
          />
          <TextField
            margin="normal"
            fullWidth
            label="E-Mail"
            name="email"
            value={registerForm.email}
            onChange={(e) => setRegisterForm({ ...registerForm, email: e.target.value })}
          />
          <TextField
            margin="normal"
            fullWidth
            label="Пароль"
            type="password"
            value={registerForm.password}
            onChange={(e) => setRegisterForm({ ...registerForm, password: e.target.value })}
          />
          {config.license
          && (
          <TextField
            margin="normal"
            fullWidth
            label="Лицензионный ключ"
            name="license"
            value={registerForm.license}
            onChange={(e) => setRegisterForm({ ...registerForm, license: e.target.value })}
          />
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 1 }}
            disabled={
              !registerForm.name
              || !registerForm.email
              || !registerForm.password
              || (config.license && !registerForm.license)
            }
            onClick={async () => {
              try {
                await signUp({
                  variables: { ...registerForm },
                  onCompleted: () => {
                    enqueueSnackbar('Регистрация прошла успешно. Пожалуйста, подтвердите свою почту.', {
                      variant: 'success',
                    });
                    // try {
                    //   signIn({
                    //     variables: { ...registerForm },
                    //     onCompleted: (data) => {
                    //       localStorage.setItem('token', data.signIn);
                    //       props.refetchUser();
                    //     },
                    //   });
                    // } catch (e) {
                    //   enqueueSnackbar(e.message, { variant: 'error' });
                    // }
                  },
                });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
              }
            }}
          >
            Зарегистрироваться
          </Button>
          <Typography variant="body2" sx={{ mt: 1 }}>
            Уже есть аккаунт?
            &nbsp;
            <Link href="login" sx={{ textDecoration: 'none' }}>Войти</Link>
          </Typography>
          <div style={{ paddingTop: 16 }}>
            <Link
              href="https://s3app.ru/privacy"
              sx={{ textDecoration: 'none' }}
              target="_blank"
            >
              Политика обработки персональных данных

            </Link>
          </div>
        </Box>
      </Box>
    </Container>
  );
}
