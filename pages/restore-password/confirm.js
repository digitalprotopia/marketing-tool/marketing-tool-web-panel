import {
  Button, Container, TextField, Typography, Box,
} from '@mui/material';
import Link from '@mui/material/Link';
import { gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useSnackbar } from 'notistack';

const CHANGE_PASSWORD_AFTER_RECOVER = gql`
    mutation($code: ID!, $password: String!) {
        changePasswordAfterRecover(code: $code, password: $password)
    }
  `;

export default function Confirm() {
  const { enqueueSnackbar } = useSnackbar();
  const [changePasswordAfterRecover] = useMutation(CHANGE_PASSWORD_AFTER_RECOVER);
  const [form, setForm] = useState({
    password: '',
    passwordConfirm: '',
  });

  const code = new URLSearchParams(window.location.search).get('code');

  return (
    <Container
      maxWidth="xs"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={(theme) => ({
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mb: `${theme.variables.header.height}px`,
        })}
      >
        <Typography component="h1" variant="h5">
          Восстановление пароля
        </Typography>
        <Box sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            fullWidth
            label="Пароль"
            type="password"
            value={form.password}
            onChange={(e) => setForm({ ...form, password: e.target.value })}
          />
          <TextField
            margin="normal"
            fullWidth
            label="Подтверждение пароля"
            type="password"
            value={form.passwordConfirm}
            onChange={(e) => setForm({ ...form, passwordConfirm: e.target.value })}
          />
          <Button
            fullWidth
            variant="contained"
            sx={{ mt: 1 }}
            disabled={!form.password
                || !form.passwordConfirm
                || form.password !== form.passwordConfirm}
            onClick={async () => {
              try {
                await changePasswordAfterRecover({ variables: { code, password: form.password } });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
                return;
              }
              enqueueSnackbar('Пароль успешно изменен', { variant: 'success' });
            }}
          >
            Сменить пароль
          </Button>
          <Box sx={{
            display: 'flex', justifyContent: 'space-between', pt: '10px', fontSize: 15,
          }}
          >
            <Link href="/login" sx={{ textDecoration: 'none' }}>Войти</Link>
            <Link href="/register" sx={{ textDecoration: 'none' }}>Зарегистрироваться</Link>
          </Box>
        </Box>
      </Box>
    </Container>
  );
}
